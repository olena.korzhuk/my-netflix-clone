import styled from "styled-components";

export const BottomMenu = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 35px;
`;

export const Footer = styled.footer`
  padding: 50px;
  background-color: rgba(0, 0, 0, .75);
`;