import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {logout} from "../../slices/authSlice";
import Header from "../../components/header/Header";
import ReactLoading from 'react-loading';
import {Container} from "../../components/Container";
import {NavLink} from "../../components/navbar/NabvarElements";
import {BottomMenu, Footer} from "./BaseElements";

const BaseLayout = (props) => {
    const dispatch = useDispatch();
    const isPending = useSelector(state => state.auth.isPending)

    const handleLogout = (e) => {
        e.preventDefault();
        dispatch(logout());
    };

    return (
        <>
            {isPending && <ReactLoading type='spinningBubbles' height={100} width={100} className='loader'/>}
            <Container isLogin={false}>
                <Header handleLogout={handleLogout}/>
                <div className="main">
                    {props.children}
                </div>
                <Footer>
                    <BottomMenu>
                        <div>
                            <NavLink to="/dashboard">
                                Home
                            </NavLink>
                        </div>
                        <div>
                            <NavLink to="/favorites">
                                Favorites
                            </NavLink>
                        </div>
                    </BottomMenu>
                    <p>Copyright &copy; 2019 Company Name: </p>
                </Footer>
            </Container>
        </>
    );
};

export default BaseLayout;