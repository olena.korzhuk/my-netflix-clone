import React from "react";
import Login from "../../components/Login";
import './LoginPage.css';
import {Container} from "../../components/Container";
import BaseLayout from "../base/BaseLayout";

const LoginPage = () => {
    return (
        <BaseLayout>
            <Container isLogin={true}>
                <Login />
            </Container>
        </BaseLayout>
    );
}

export default LoginPage;