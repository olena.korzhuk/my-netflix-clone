import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getFavoritesFilmsIds} from "../slices/favoriteFilmsSlice";
import {selectFilteredFavoriteFilms} from "../selectors/favoritesSelector";
import BaseLayout from "./base/BaseLayout";
import Movies from "../components/movies/Movies";
import {useLocation} from "react-router-dom";
import { doc, onSnapshot } from "firebase/firestore";
import app, {db} from "../firebase/firebase";

const FavoritesPage = () => {
    const dispatch = useDispatch();
    const location = useLocation();
    const favoriteFilms = useSelector(selectFilteredFavoriteFilms);
    const uid = app.auth().currentUser.uid;

    useEffect(() => {
        dispatch(getFavoritesFilmsIds());

        const unsub = onSnapshot(doc(db, "users", uid), () => {
            dispatch(getFavoritesFilmsIds());
        });

        return () => {
            unsub();
        }
    }, [])

    return (
        <BaseLayout>
            {favoriteFilms && favoriteFilms.length > 0
                ? <Movies
                    movies={favoriteFilms}
                    currentPath={location.pathname}/>
                : <p>No Favorite shows on your list</p>
            }
        </BaseLayout>
    );
};

export default FavoritesPage;