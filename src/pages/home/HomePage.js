import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getFilms, getFilmsLikes} from "../../slices/filmsSlice";
import {selectFilteredAllFilms} from "../../selectors/filmsSelector";
import BaseLayout from "../base/BaseLayout";
import Movies from "../../components/movies/Movies";
import {useLocation} from 'react-router-dom'
import {getFavoritesFilmsIds, getLikedFilmsIds} from "../../slices/favoriteFilmsSlice";

const HomePage = () => {
        const dispatch = useDispatch();
        const films = useSelector(selectFilteredAllFilms)
        const location = useLocation();

        useEffect(() => {
            dispatch(getFilms());
            dispatch(getFilmsLikes());
            dispatch(getLikedFilmsIds());
            dispatch(getFavoritesFilmsIds());
        }, [])

        return (
            <BaseLayout>
                {films &&
                <Movies
                    movies={films}
                    currentPath={location.pathname}
                />}
            </BaseLayout>
        );
    }
;

export default HomePage;