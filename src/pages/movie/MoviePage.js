import React, {useEffect, useState} from "react";
import BaseLayout from "../base/BaseLayout";
import {useParams} from 'react-router-dom';
import {
    addToFavorites,
    removeFromFavorites
} from "../../slices/favoriteFilmsSlice";
import {useDispatch, useSelector} from "react-redux";
import styled from "styled-components";
import {
    ButtonsWrap,
    FavoritesButton, LikeButton,
    MovieInfoContainer,
    MovieInfoSection,
    MovieSummarySection,
    MovieVisualSection, StyledIcon
} from "./MoviePageELements";
import {getLikesByFilmId} from "../../selectors/filmsSelector";
import {doc, onSnapshot} from "firebase/firestore";
import {db} from "../../firebase/firebase";
import {getFilmsLikes} from "../../slices/filmsSlice";
import {getFilm, likeFilm, unlikeFilm} from "../../slices/currentFilmSlice";

const ImageWrap = styled.div`
  width: 450px;
  height: 650px;
`;

const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

const MoviePage = () => {
    const {id} = useParams();
    const [isFavorite, setIsFavorite] = useState(false);
    const [isLiked, setIsLiked] = useState(false);
    const favoriteFilmsIds = useSelector(state => state.favoriteFilms.favoriteFilmsIds);
    const likedFilmsIds = useSelector(state => state.favoriteFilms.likedFilmsIds);
    const movieInfo = useSelector(state => state.currentFilm.filmInfo);
    const dispatch = useDispatch();
    const removeTags = (str) => str.replace(/(<([^>]+)>)/gi, "");
    const likesCount = useSelector(getLikesByFilmId(id));

    useEffect(() => {
        dispatch(getFilm(id));

        // if (favoriteFilmsIds.includes(movieInfo.id)) {
        //     setIsFavorite(true);
        // }
    }, []);



    useEffect(() => {
        if(movieInfo){
            if (likedFilmsIds.includes(movieInfo.id)) {
                setIsLiked(true);
            } else {
                setIsLiked(false);
            }

            if (favoriteFilmsIds.includes(movieInfo.id)) {
                setIsFavorite(true);
            }
        }

        const unsub = onSnapshot(doc(db, "movies", id), () => {
            dispatch(getFilmsLikes());
        });

        return () => {
            unsub();
        }
    }, [movieInfo])

    const onFavoritesClickHandler = () => {
        if (isFavorite) {
            dispatch(removeFromFavorites(movieInfo));
            setIsFavorite(false);
        } else {
            dispatch(addToFavorites(movieInfo));
            setIsFavorite(true);
        }
    }

    const onLikeButtonClickHandler = () => {
        if (isLiked) {
            dispatch(unlikeFilm(id));
            // dispatch(updateLikedStatus(false));
            setIsLiked(false);
        } else {
            dispatch(likeFilm(id));
            // dispatch(updateLikedStatus(true));
            setIsLiked(true);
        }
    }

    return movieInfo && (
        <BaseLayout>
            <MovieInfoContainer>
                <MovieVisualSection>
                    <h2>{movieInfo.name}</h2>
                    <ImageWrap>
                        <Image src={movieInfo.image.original} alt={movieInfo.name}/>
                    </ImageWrap>
                    <ButtonsWrap>
                        <FavoritesButton
                            onClick={onFavoritesClickHandler}>
                            {isFavorite ? 'Remove from favorites' : 'Add to Favorites'}
                        </FavoritesButton>
                        <div>
                            <LikeButton onClick={onLikeButtonClickHandler}>
                                <StyledIcon isLiked={isLiked}/>
                            </LikeButton>
                            <span>{isLiked
                                ? `You and ${likesCount - 1} others liked`
                                : `${likesCount} people liked`}</span>
                        </div>

                    </ButtonsWrap>
                </MovieVisualSection>

                <MovieSummarySection>
                    <p>{removeTags(movieInfo.summary)}</p>
                </MovieSummarySection>

                <MovieInfoSection>
                    <div>
                        <h3>Show Info</h3>
                        <div>Average run time: {movieInfo.averageRuntime}</div>
                        <div>Status: {movieInfo.ended}</div>
                        <div>Genres: {movieInfo.genres}</div>
                        <div>Language: {movieInfo.language}</div>
                    </div>
                </MovieInfoSection>
            </MovieInfoContainer>
        </BaseLayout>
    )
};

export default MoviePage;