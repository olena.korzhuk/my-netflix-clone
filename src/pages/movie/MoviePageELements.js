import styled from "styled-components";
import {MdThumbUp} from "react-icons/md";

export const MovieInfoContainer = styled.div`
  display: grid;
  grid-gap: 50px;
  grid-template-columns: repeat(auto-fit, minmax(400px, 1fr));
  width: 85%;
  margin: 0 auto;
  text-align: justify;
`;

const Section = styled.section`
  text-align: left;
  font-size: 1.2rem;
  line-height: 2rem;
  padding: 100px 0;
  color: darkgray;
`;

export const MovieSummarySection = styled(Section)`
  text-align: left;
`;

export const MovieInfoSection = styled(Section)`

`;

export const MovieVisualSection = styled.section`
  padding: 50px 0 100px;

  > h2 {
    margin-bottom: 20px;
  }

  > div {
    margin-bottom: 20px;
  }
`;

export const ButtonsWrap = styled.div`
  width: 450px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const FavoritesButton = styled.button`
  padding: 10px;
  font-size: 1rem;
  font-weight: bold;
  color: #333333;
  cursor: pointer;
`;

export const LikeButton = styled.button`
  background: transparent;
  padding: 10px;
  border: none;
  outline: none;
`

export const StyledIcon = styled(MdThumbUp)`
  fill: ${props => props.isLiked ? '#719feb' : 'grey'};
  font-size: 1.5rem;
`;