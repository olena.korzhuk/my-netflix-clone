import {createDraftSafeSelector} from "@reduxjs/toolkit";

export const favoritesSelector = (state) => state.favoriteFilms;

export const filmsData = createDraftSafeSelector(
    favoritesSelector,
    favoriteFilms => {
        return favoriteFilms.films;
    }
);

export const selectAllFavoriteFilms = (state) => {
    const allFilms = state.films.films;
    const favoriteFilmsIds = state.favoriteFilms.favoriteFilmsIds;

    return allFilms.filter(film => {
        return favoriteFilmsIds.includes(film.id);
    })
};

export const selectFilteredFavoriteFilms = (state) => {
    const allFavorites = selectAllFavoriteFilms(state);
    const searchTerm = state.search;

    return allFavorites.filter(film => film.name.toLowerCase().includes(searchTerm.toLowerCase()))
};