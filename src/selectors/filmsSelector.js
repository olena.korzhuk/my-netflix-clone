import {createDraftSafeSelector} from "@reduxjs/toolkit";
import {useSelector} from "react-redux";

export const filmsSelector = (state) => state.films;

export const fetchingStatus = createDraftSafeSelector(
    filmsSelector,
    films => {
        return films.status;
    }
);

export const filmsData = createDraftSafeSelector(
    filmsSelector,
    films => {
        return films.films;
    }
);

export const filmsLikesSelector = createDraftSafeSelector(
    filmsSelector,
    films => {
        return films.filmsLikes;
    }
);

export const selectFilteredAllFilms = (state) => {
    const allFilms = state.films.films;
    const searchTerm = state.search;

    return allFilms.filter((films) => films.name.toLowerCase().includes(searchTerm.toLowerCase()));
};

export const getLikesByFilmId = (id) => (state) => {
    const allFilmsLikes = state.films.filmsLikes;

    const likes = allFilmsLikes.find(el => el.id === parseInt(id));
    return likes ? likes.likesCount : 0;
}