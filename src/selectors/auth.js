import {createDraftSafeSelector} from "@reduxjs/toolkit";

export const authSelector = (state) => state.auth;

export const isUserAuthenticatedSelector = createDraftSafeSelector(
    authSelector,
    auth => {
        return auth.isAuthenticated;
    }
);

export const isPendingSelector = createDraftSafeSelector(
    authSelector,
    auth => {
        return auth.isPending;
    }
);

export const userSelector = createDraftSafeSelector(
    authSelector,
    auth => {
        return auth.user;
    }
);

export const errorSelector = createDraftSafeSelector(authSelector, auth => {
    return auth.error;
});

export const providedPasswordSelector = createDraftSafeSelector(
    authSelector,
    auth => {
        return auth.password;
    }
);

export const tokenSelector = createDraftSafeSelector(
    authSelector,
    auth => {
        return auth.token;
    }
);