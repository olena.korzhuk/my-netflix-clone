import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import app, {db} from '../firebase/firebase';
import { doc, getDoc, updateDoc } from "firebase/firestore";
import firebase from "firebase/compat";

export const addToFavorites = createAsyncThunk(
    'favoriteFilms/addToFavorites',
    async (film, thunkAPI) => {
        const uid = app.auth().currentUser;
        const docRef = doc(db, 'users', uid.uid);
        const docSnap = await getDoc(docRef);

        if (docSnap.exists()) {
            console.log("Document data:", docSnap.data());
            try{
                await updateDoc(docRef, {
                    favorites: firebase.firestore.FieldValue.arrayUnion(film.id)
                })
            }catch (e) {
                console.log('ERROR',e);
            }
        } else {
            console.log("No such document!");
        }
        return film;
    }
);

export const removeFromFavorites = createAsyncThunk(
    'favoriteFilms/removeFromFavorites',
    async (film, thunkAPI) => {
        const uid = app.auth().currentUser;
        const docRef = doc(db, 'users', uid.uid);
        const docSnap = await getDoc(docRef);
        if (docSnap.exists()) {
            try{
                await updateDoc(docRef, {
                    favorites: firebase.firestore.FieldValue.arrayRemove(film.id)
                })
            }catch (e) {
                console.log('ERROR',e);
            }
        } else {
            console.log("No such document!");
        }
        return film;
    }
);

export const getFavoritesFilmsIds = createAsyncThunk(
    'favoriteFilms/getFavoritesFilmsIds',
    async (_, thunkAPI) => {
        const uid = app.auth().currentUser;
        const docRef = doc(db, 'users', uid.uid);
        // try{
            const docSnap = await getDoc(docRef);
            const f = docSnap.data().favorites;
            return f;
        // }catch (e){
        //     return thunkAPI.rejectWithValue({ error: e.message });
        // }
    }
);

export const getLikedFilmsIds = createAsyncThunk(
    'currentUser/getLikedFilmsIds',
    async (_, thunkAPI) => {
        const uid = app.auth().currentUser;
        const docRef = doc(db, 'users', uid.uid);
        // try{
        const docSnap = await getDoc(docRef);
        const f = docSnap.data().likedFilms;
        return f;
        // }catch (e){
        //     return thunkAPI.rejectWithValue({ error: e.message });
        // }
    }
);

export const favoriteFilmsSlice = createSlice({
    name: 'favoriteFilms',
    initialState: {
        favoriteFilmsIds: [],
        likedFilmsIds: [],
        status: null
    },
    reducers: {
        addFavoriteFilm: (state, action) => {
            state.favoriteFilms.push(action.payload);
            return state;
        }
    },
    extraReducers: (builder) => {
        // builder.addCase(addToFavorites.pending, (state, action) => {
        //     state.status = 'loading';
        // });
        // builder.addCase(addToFavorites.fulfilled, (state, action) => {
        //     state.status = 'success';
        //     state.favoriteFilms.push(action.payload);
        //     return state;
        // });
        builder.addCase(getFavoritesFilmsIds.pending, (state, action) => {
            state.status = 'loading';
        });
        builder.addCase(getFavoritesFilmsIds.fulfilled, (state, action) => {
           state.status = 'success';
           state.favoriteFilmsIds = action.payload;
        });
        builder.addCase(removeFromFavorites.pending, (state, action) => {
            state.status = 'loading';
        });
        builder.addCase(getLikedFilmsIds.pending, (state, action) => {
            state.status = 'loading';
        });
        builder.addCase(getLikedFilmsIds.fulfilled, (state, action) => {
            state.status = 'success';
            state.likedFilmsIds = action.payload;
        });
        // builder.addCase(removeFromFavorites.fulfilled, (state, action) => {
        //     state.status = 'success';
        //     const t = action.payload;
        //     console.log('FAVORITES',action.payload);
        //     debugger
        //     // state.favoriteFilmsIds = action.payload;
        // });
    }
});

// export const { addFavoriteFilm } = favoriteFilmsSlice.actions
export default favoriteFilmsSlice.reducer;