import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import firebase from "firebase/compat";
import {doc, getDoc, updateDoc} from "firebase/firestore";
import {db} from "../firebase/firebase";

const baseUrl = 'https://api.tvmaze.com/';
const filmsRoute = 'shows';

export const getFilms = createAsyncThunk(
    'films/getFilms',
    async (_, thunkAPI) => {
        const response = await fetch(`${baseUrl}${filmsRoute}`);
        const dataParsed = await response.json();
        return dataParsed;
    }
);

export const getFilmsLikes = createAsyncThunk(
    'films/getFilmsLike',
    async (_, thunkAPI) => {
        const snapshot = await firebase.firestore().collection('movies').get();
        if (snapshot.empty || snapshot.size === 0) {
            return {};
        }
        return snapshot.docs.reduce((acc, doc) => {
            const obj = {
                id: parseInt(doc.id),
                likesCount: doc.data().likes
            };
            acc.push(obj);
            return acc;
        }, []);
    }
);

// export const likeFilm = createAsyncThunk(
//     'films/likeFilm',
//     async (filmId, thunkAPI) => {
//         debugger
//         const docRef = doc(db, 'movies', filmId);
//         debugger
//         const docSnap = await getDoc(docRef);
//
//         debugger
//         if (docSnap.exists()) {
//             console.log("Document data:", docSnap.data());
//             try{
//                 await updateDoc(docRef, {
//                     likes: firebase.firestore.FieldValue.increment(1)
//                 })
//                 debugger
//             }catch (e) {
//                 console.log('ERROR',e);
//             }
//         } else {
//             console.log("No such document!");
//         }
//     }
// );

// export const unlikeFilm = createAsyncThunk(
//     'films/unlikeFilm',
//     async (filmId, thunkAPI) => {
//         const docRef = doc(db, 'movies', filmId);
//         const docSnap = await getDoc(docRef);
//
//         debugger
//         if (docSnap.exists()) {
//             console.log("Document data:", docSnap.data());
//             try{
//                 await updateDoc(docRef, {
//                     likes: firebase.firestore.FieldValue.increment(-1)
//                 })
//                 debugger
//             }catch (e) {
//                 console.log('ERROR',e);
//             }
//         } else {
//             console.log("No such document!");
//         }
//     }
// );

export const filmsSlice = createSlice({
    name: 'films',
    initialState: {
        films: [],
        status: null,
        filmsLikes: []
    },
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getFilms.pending, (state, action) => {
            state.status = 'loading';
        });
        builder.addCase(getFilms.fulfilled, (state, action) => {
            state.status = 'success';
            console.log(action.payload);
            state.films = action.payload;
        });
        builder.addCase(getFilms.rejected, (state, action) => {
            state.status = 'failure';
        });
        builder.addCase(getFilmsLikes.fulfilled, (state, action) => {
            state.status = 'success';
            state.filmsLikes = action.payload;
        });
    }
});


export default filmsSlice.reducer;