import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import firebase from "firebase/compat";
import {doc, getDoc, updateDoc} from "firebase/firestore";
import app, {db} from "../firebase/firebase";

const baseUrl = 'https://api.tvmaze.com/';
const filmsRoute = 'shows';

export const getFilm = createAsyncThunk(
    'films/getFilm',
    async (filmId, thunkAPI) => {
        const response = await fetch(`${baseUrl}${filmsRoute}/${filmId}`);
        return await response.json();
    }
);

export const likeFilm = createAsyncThunk(
    'films/likeFilm',
    async (filmId, thunkAPI) => {
        const docRef = doc(db, 'movies', filmId);
        const docSnap = await getDoc(docRef);

        if (docSnap.exists()) {
            try {
                await updateDoc(docRef, {
                    likes: firebase.firestore.FieldValue.increment(1)
                })
            } catch (e) {
                console.log('ERROR', e);
            }
        } else {
            try {
                await db.collection('movies').doc(filmId).set({
                    likes: 1
                }, {merge: true});
            } catch (e) {
                console.log('ERROR', e);
            }
        }

        const uid = app.auth().currentUser;
        const userDocRef = doc(db, 'users', uid.uid);
        const userDocSnap = await getDoc(userDocRef);

        if (userDocSnap.exists()) {
            try {
                await updateDoc(userDocRef, {
                    likedFilms: firebase.firestore.FieldValue.arrayUnion(parseInt(filmId))
                })
            } catch (e) {
                console.log('ERROR', e);
            }
        } else {
            console.log("No such document!");
        }
    }
);

export const unlikeFilm = createAsyncThunk(
    'films/unlikeFilm',
    async (filmId, thunkAPI) => {
        const docRef = doc(db, 'movies', filmId);
        const docSnap = await getDoc(docRef);

        if (docSnap.exists()) {
            try {
                await updateDoc(docRef, {
                    likes: firebase.firestore.FieldValue.increment(-1)
                })
            } catch (e) {
                console.log('ERROR', e);
            }
        } else {
            console.log("No such document!");
        }

        const uid = app.auth().currentUser;
        const userDocRef = doc(db, 'users', uid.uid);
        const userDocSnap = await getDoc(docRef);
        if (userDocSnap.exists()) {
            try {
                await updateDoc(userDocRef, {
                    likedFilms: firebase.firestore.FieldValue.arrayRemove(parseInt(filmId))
                })
            } catch (e) {
                console.log('ERROR', e);
            }
        } else {
            console.log("No such document!");
        }
    }
);

export const currentFilmSLice = createSlice({
    name: 'currentFilm',
    initialState: {
        filmInfo: null,
        status: null,
    },
    reducers: {
        updateLikedStatus(state, action) {
            state.isLiked = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(getFilm.pending, (state, action) => {
            state.status = 'loading';
        });
        builder.addCase(getFilm.fulfilled, (state, action) => {
            state.status = 'success';
            state.filmInfo = action.payload;
        });
        builder.addCase(likeFilm.fulfilled, (state, action) => {
        });
        builder.addCase(unlikeFilm.fulfilled, (state, action) => {
        });
    }
});

export default currentFilmSLice.reducer;