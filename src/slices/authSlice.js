import {
    createAsyncThunk, createSlice
} from '@reduxjs/toolkit';
import {auth} from "../firebase/firebase";

const initialState = {
    user: null,
    isAuthenticated: null,
    error: null,
    isPending: true,
    token: null
};

export const login = createAsyncThunk(
    'login',
    async (userData, thunkAPI) => {
        const {email, password} = userData;
        try {
            const response = await auth.signInWithEmailAndPassword(email, password);
            const user = auth.currentUser;
            return response;
        } catch (error) {
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
);

export const refresh = createAsyncThunk(
    'refresh',
    async (_, thunkAPI) => {
        try {
            const response = await auth.currentUser.getIdToken();
            const user = auth.currentUser;
            return response
        } catch (error) {
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
);

export const logout = createAsyncThunk(
    'logout',
    async (_, thunkAPI) => {
        try {
            const response = await auth.signOut();
            const user = auth.currentUser;
            return response;
        } catch (error) {
            return thunkAPI.rejectWithValue({ error: error.message });
        }
    }
);

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        refreshAuth: (state, action) => {
            state.user = action.payload.user;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(login.fulfilled, (state, action) => {
            state.user = action.payload;
            state.isAuthenticated = true;
            state.isPending = false;
        });
        builder.addCase(login.rejected, (state, action) => {
            state.error = action.error.message;
            state.isPending = false;
        });
        builder.addCase(login.pending, (state, action) => {
            state.isPending = true;
        });
        builder.addCase(refresh.fulfilled, (state, action) => {
            state.token = action.payload;
            state.isAuthenticated = true;
            state.user = action.payload.user;
            state.isPending = false;
        });
        builder.addCase(logout.fulfilled, (state, action) => {
            state.user = null;
            state.isAuthenticated = null;
            state.password = null;
            state.isPending = null;
            state.token = null;
        });
        builder.addCase(logout.pending, (state, action) => {
            state.isPending = true;
        });
        builder.addCase(logout.rejected, (state, action) => {
            console.log(action.error);
            state.error = action.error;
            state.isPending = null;
        });
    }
});