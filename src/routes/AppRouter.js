import React, {useEffect} from "react";
import {Route, Switch, Redirect} from 'react-router-dom'
import {useDispatch, useSelector} from "react-redux";
import {
    isPendingSelector,
    isUserAuthenticatedSelector,
    tokenSelector
} from "../selectors/auth";
import {privateRoutes, publicRoutes} from "./routes";
import firebase from "firebase/compat";
import {logout, refresh} from "../slices/authSlice";
import ReactLoading from 'react-loading';
import Header from "../components/header/Header";

const AppRouter = () => {
    const isAuthenticated = useSelector(isUserAuthenticatedSelector);
    const isPending = useSelector(isPendingSelector);
    const token = useSelector(tokenSelector);
    const dispatch = useDispatch();

    useEffect(() => {
        firebase.auth().onAuthStateChanged((user) => {
            if (user && !isAuthenticated) {
                dispatch(refresh())
            }
            if (!user && !isAuthenticated) {
                dispatch(logout());
            }
        })
    }, [])

    return isPending && !isAuthenticated ?
        (<ReactLoading type='spinningBubbles' height={100} width={100} color='black' className='loader'/>) : isAuthenticated ?
            (
                <>
                    {/*<Header />*/}
                    <Switch>
                        {privateRoutes.map(({path, Component}) => <Route path={path} component={Component} exact/>)}
                        <Redirect to='/dashboard'/>
                    </Switch>
                </>

            ) :
            (
                <>
                    {/*<Header />*/}
                    <Switch>
                        {publicRoutes.map(({path, Component}) => <Route path={path} component={Component} exact/>)}
                        <Redirect to='/'/>
                    </Switch>
                </>
            )
};

export default AppRouter;