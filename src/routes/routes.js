import LoginPage from "../pages/login/LoginPage";
import HomePage from "../pages/home/HomePage";
import FavoritesPage from "../pages/FavoritesPage";
import MoviePage from "../pages/movie/MoviePage";

export const publicRoutes = [
    {
        path: '/',
        Component: LoginPage
    },
];

export const privateRoutes = [
    {
        path: '/dashboard',
        Component: HomePage
    },
    {
        path: '/favorites',
        Component: FavoritesPage
    },
    {
        path: '/favorites/:id',
        Component: MoviePage
    },
    {
        path: '/dashboard/:id',
        Component: MoviePage
    },
];