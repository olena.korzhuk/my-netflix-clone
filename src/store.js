import {configureStore} from '@reduxjs/toolkit';
import {authSlice} from './slices/authSlice';
import {filmsSlice} from './slices/filmsSlice';
import {favoriteFilmsSlice} from "./slices/favoriteFilmsSlice";
import {searchSlice} from "./slices/searchSlice";
import {currentFilmSLice} from "./slices/currentFilmSlice";

const saveState = (state) => {
    try{
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    }catch (err){
        console.error(err);
    }
};

export const loadState = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if(serializedState === null){
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (e) {
        console.error(e);
    }
};

const preloadedState = loadState();

export const store = configureStore({
    reducer: {
        auth: authSlice.reducer,
        films: filmsSlice.reducer,
        favoriteFilms: favoriteFilmsSlice.reducer,
        search: searchSlice.reducer,
        currentFilm: currentFilmSLice.reducer,
    },
    preloadedState
});

store.subscribe(() => {
    saveState({
        favoriteFilms: store.getState().favoriteFilms,
        films: store.getState().films,
        currentFilm: store.getState().currentFilm,
    });
});


