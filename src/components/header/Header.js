import React from "react";
import {HeaderElement} from "./HeaderElement";
import Navbar from "../navbar/Navbar";

const Header = (props) => {
    const {handleLogout} = props;

    return (
        <HeaderElement>
            <Navbar handleLogout={handleLogout}/>
        </HeaderElement>
    );
};

export default Header;