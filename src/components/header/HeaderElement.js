import styled from 'styled-components';

export const HeaderElement = styled.div`
  height: 85px;
  position: fixed;
  left: 0;
  right: 0;
  transition: background-color 0.3s ease-in;
`;