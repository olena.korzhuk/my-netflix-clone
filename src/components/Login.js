import React, { useState} from "react";
import {useDispatch} from "react-redux";

import {login} from "../slices/authSlice";

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();

    const handleEmailInput = (e) => {
        setEmail(e.target.value)
    }

    const handlePasswordInput = (e) => {
        setPassword(e.target.value)
    }

    const handleLogin = (e) => {
        e.preventDefault();
        const userData = {
            email,
            password,
        };
        dispatch(login(userData));
    }

    return (
        <div className="form-container">
            <form action="" onSubmit={handleLogin}>
                <h1 className="login-form_header">Sign In</h1>
                <div className="input-group">
                    <input
                        type="text"
                        id="loginId"
                        placeholder="Email"
                        value={email}
                        onChange={handleEmailInput}/>
                </div>
                <div className="input-group">
                    <input
                        type="text"
                        id="passwordId"
                        placeholder="Password"
                        value={password}
                        onChange={handlePasswordInput}/>
                </div>
                <button type="submit" id="submit">Sign In</button>
            </form>
            <a href="">Need help?</a>
            <div className="form-footer">
                <p>New to Netflix?</p>
                <a href="">Sign Up Now</a>
            </div>
        </div>
    );
}

export default Login;