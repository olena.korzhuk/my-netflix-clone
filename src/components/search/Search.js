import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {setSearchTerm} from "../../slices/searchSlice";
import './Search.css';
import {FaSearch} from 'react-icons/fa';

const Search = () => {
    const searchTerm = useSelector(state => state.search);
    const dispatch = useDispatch();

    const handleSearchInput = (e) => {
        const userInput = e.target.value;
        dispatch(setSearchTerm(userInput));
    };

    return (
        <div id="search-container">
            <FaSearch id="search-icon" alt="Search"/>
            <input
                id="search"
                type="text"
                value={searchTerm}
                placeholder='Search'
                onChange={handleSearchInput}/>
        </div>
    );
};

export default Search;