import styled from "styled-components";
import img from "../images/login_page_bg_image_large.jpg";

export const Container = styled.div`
  background: ${props => props.isLogin
    ? `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${img})`
    : '#141414'
  
  };
  background-size: cover;
  width: 100%;
  color: white;
  overflow: auto;
`;