import React from "react";

const SignUp = () => {
    return (
        <div className="form-container">
            <form action="">
                <h1 className="login-form_header">Sign Up</h1>
                <div className="input-group">
                    <input type="text" id="loginId" placeholder="Email"/>
                </div>
                <div className="input-group">
                    <input type="text" id="passwordId" placeholder="Password"/>
                </div>
                <div className="input-group">
                    <input type="text" id="confirmPasswordId" placeholder="Confirm password"/>
                </div>
                <button type="submit" id="submit">Sign Up</button>
            </form>
            <a href="">Need help?</a>
            <div className="form-footer">
                <p>Already have an account?</p>
                <a href="">Login</a>
            </div>
        </div>
    );
}

export default SignUp;