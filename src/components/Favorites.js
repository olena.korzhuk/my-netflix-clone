import React from 'react';
import Movie from "./movie/Movie";
import {useSelector} from "react-redux";

const Favorites = () => {
    const {favoriteFilms} = useSelector(state => state.favoriteFilms);

    return (
        <div className='films-container'>
            {favoriteFilms && favoriteFilms.map((film) => {
                return <Movie
                    key={film.id}
                    name={film.name}
                    year={film.premiered}
                    imgUrl={film.image.medium} />
                    // onCLickHandler={() => onAddToFavoritesHandler(film)}/>
            })}
        </div>
    );
};

export default Favorites;