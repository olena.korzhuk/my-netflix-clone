import React from "react";
import './Movie.css';
import {Link, useLocation} from "react-router-dom";
import {removeFromFavorites} from "../../slices/favoriteFilmsSlice";
import {useDispatch} from "react-redux";

const Movie = (props) => {
    const {imgUrl, name, moviePath, movie} = props;
    const location = useLocation();
    const dispatch = useDispatch();

    const onRemoveFromFavoritesHandler = () => {
        dispatch(removeFromFavorites(movie));
        // setIsFavorite(false);
    }

    return (
        <div className='movieWrap'>
            <div className="imgWrap">
                <Link to={moviePath}>
                    <img src={imgUrl} alt={name}/>
                </Link>
            </div>
            <div className="content">
                <h3>{name}</h3>
                {location.pathname === '/favorites' &&
                <button onClick={onRemoveFromFavoritesHandler}>
                    Remove from favorites
                </button>}
            </div>
        </div>
    );
}

export default Movie;