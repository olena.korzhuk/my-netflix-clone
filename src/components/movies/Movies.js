import React from 'react';
import Movie from "../movie/Movie";
import {MoviesContainer} from "./MoviesContainer";
import {useLocation} from "react-router-dom";

const Movies = (props) => {
    const {movies, currentPath} = props;

    console.log(currentPath)

    return (
        <MoviesContainer>
            {movies.map((movie) => {
                return <Movie
                    key={movie.id}
                    name={movie.name}
                    year={movie.premiered}
                    imgUrl={movie.image.medium}
                    moviePath={`${currentPath}/${movie.id}`}
                    movie={movie}
                />
            })}
        </MoviesContainer>
    );
};

export default Movies;