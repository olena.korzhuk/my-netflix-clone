import styled from "styled-components";

export const MoviesContainer = styled.div`
  margin: 20px auto;
  width: 90%;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(170px, 1fr));
  grid-auto-rows: 325px;
  grid-gap: 5px;
`;