import React from "react";
import {Bars, Nav, NavLink, NavLogo, NavMenu} from "./NabvarElements";
import {isUserAuthenticatedSelector} from "../../selectors/auth";
import {useSelector} from "react-redux";
import Search from "../search/Search";

const Navbar = (props) => {
    const {handleLogout} = props;
    const isAuthenticated = useSelector(isUserAuthenticatedSelector);

    return (
        <>
            <Nav isAuthenticated={isAuthenticated}>
                <NavLogo to="/dashboard">MY NETFLIX</NavLogo>
                {isAuthenticated &&
                <>
                    <Bars/>
                    <NavMenu>
                        <Search/>
                        <NavLink to="/dashboard">
                            Home
                        </NavLink>
                        <NavLink to="/favorites">
                            Favorites
                        </NavLink>
                        <NavLink to="/" onClick={handleLogout}>
                            Logout
                        </NavLink>
                    </NavMenu>
                </>
                }
            </Nav>
        </>
    );
};

export default Navbar;