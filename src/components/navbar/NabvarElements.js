import {FaBars} from "react-icons/fa";
import {NavLink as Link} from "react-router-dom";
import styled from "styled-components";

export const Nav = styled.nav`
  background-color: transparent;
  background-image: ${props => props.isAuthenticated && 'linear-gradient(147deg, #000000 0%, #434343 74%)'};
  height: 60px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 50px;
  z-index: 2;
  position: fixed;
  left: 0;
  right: 0;
  transition: background-color 0.3s ease-in;
`;

export const NavLogo = styled(Link)`
  cursor: pointer;
  color: red;
  font-size: 2rem;
  text-decoration: none;
  font-weight: bold;
`;

export const NavLink = styled(Link)`
  color: #fff;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;

  &.active {
    font-weight: bold;
  }

  &:hover {
    color: dimgrey;
  }
`;

export const Bars = styled(FaBars)`
  display: none;
  color: #fff;
  @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`;

export const NavMenu = styled.div`
  display: flex;
  align-items: center;
  margin-right: -24px;

  //@media screen and (max-width: 768px) {
  //  display: none;
  //}
`;

export const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  margin-right: 24px;

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const NavBtnLink = styled(Link)`
  border-radius: 4px;
  background: transparent;
  padding: 10px 22px;
  color: #fff;
  outline: none;
  border: 1px solid #fff;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;
  margin-left: 24px;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: #fff;
    color: #808080;
  }
`;